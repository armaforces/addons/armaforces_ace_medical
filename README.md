All bandages changed:
```
Super efficiency:
Avulsion, Contusion, Crush - elastyki
Abrassion, cut, laceration - field dressing
puncture, velocity - Packing/quick clot

Increased efficiency:
puncture, velocity - elastic/field

Standard efficiency:
remaining combos, stats still increased
```

[Sheet](https://docs.google.com/spreadsheets/d/1gWi1LKpMh2if_LRh9ikBWmDUVOwMAxsCovGmuUoqGCc/edit#gid=0) for calculating various things:
- Medication (eg. morphine) usage effect on HR & BP