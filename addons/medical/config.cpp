class CfgPatches
{
    class ArmaForces_ace_medical
    {
        name = "ArmaForces - ACE Medical";
        units[] = {"ACE_medicalSupplyCrate","ACE_medicalSupplyCrate_advanced","ACE_fieldDressingItem","ACE_packingBandageItem","ACE_elasticBandageItem","ACE_tourniquetItem","ACE_morphineItem","ACE_atropineItem","ACE_epinephrineItem","ACE_plasmaIVItem","ACE_bloodIVItem","ACE_salineIVItem","ACE_quikclotItem","ACE_personalAidKitItem","ACE_surgicalKitItem","ACE_bodyBagItem","ACE_bodyBagObject","ACE_adenosineItem","ACE_apapItem"};
        weapons[] = {"ACE_fieldDressing","ACE_packingBandage","ACE_elasticBandage","ACE_tourniquet","ACE_morphine","ACE_atropine","ACE_epinephrine","ACE_plasmaIV","ACE_plasmaIV_500","ACE_plasmaIV_250","ACE_bloodIV","ACE_bloodIV_500","ACE_bloodIV_250","ACE_salineIV","ACE_salineIV_500","ACE_salineIV_250","ACE_quikclot","ACE_personalAidKit","ACE_surgicalKit","ACE_bodyBag","ACE_adenosine","ACE_fentanyl","ACE_naloxone","ACE_apap"};
        requiredVersion = 1.82;
        requiredAddons[] = {"ace_medical"};
        author = "ArmaForces";
        authors[] = {"3Mydlo3", "Madin"};
        url = "https://armaforces.com";
        version = "3.12.2.33";
        versionStr = "3.12.2.33";
        versionAr[] = {3,12,2,33};
    };
};

#include "script_macros.hpp"

#include "CfgFunctions.hpp"
#include "CfgVehicles.hpp"
#include "CfgWeapons.hpp"
#include "ACE_Medical_Actions.hpp"
#include "ACE_Medical_Advanced.hpp"
#include "ACE_Medical_Treatments.hpp"
