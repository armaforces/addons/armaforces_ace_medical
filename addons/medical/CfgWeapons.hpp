class CfgWeapons {
    class ACE_morphine;
    class ACE_atropine;
    class CBA_MiscItem_ItemInfo;

    class ACE_fentanyl: ACE_morphine {
        author = "ArmaForces";
        displayName = $STR_ArmaForces_Medical_Fentanyl_Display;
        descriptionShort = $STR_ArmaForces_Medical_Fentanyl_Desc_Short;
        descriptionUse = $STR_ArmaForces_Medical_Fentanyl_Desc_Use;
    };
    class ACE_naloxone: ACE_atropine {
        author = "ArmaForces";
        displayName = $STR_ArmaForces_Medical_Naloxone_Display;
        descriptionShort = $STR_ArmaForces_Medical_Naloxone_Desc_Short;
        descriptionUse = $STR_ArmaForces_Medical_Naloxone_Desc_Use;
    };
    class ACE_apap: ACE_morphine {
        author = "ArmaForces";
        displayName = $STR_ArmaForces_Medical_Apap_Display;
        picture = SET_ICON(apap);
        model = "\A3\Structures_F_EPA\Items\Medical\PainKillers_F.p3d";
        descriptionShort = $STR_ArmaForces_Medical_Apap_Desc_Short;
        descriptionUse = $STR_ArmaForces_Medical_Apap_Desc_Use;
        class ItemInfo: CBA_MiscItem_ItemInfo {
            mass = 0.5;
        };
    };
};