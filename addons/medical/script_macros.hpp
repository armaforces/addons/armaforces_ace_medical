// Encloses param with ""
#define STRINGYFY(s) #s

#define SET_ICON(file) STRINGYFY(\armaforces\medical\ui\icons\##file##.paa)