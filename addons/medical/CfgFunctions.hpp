class CfgFunctions {
    class armaforces_ace_medical {
        class functions {
            file = "armaforces\medical\functions";
            class adenosine {};
            class adenosineLocal {};
            class epinephrine {};
            class epinephrineLocal {};
            class epinephrineLocalEffectLoop {};
        };
    };
    class ace_medical {
        class functions {
            file = "armaforces\medical\functions";
            class handleCreateLitter {};
        };
    };
};