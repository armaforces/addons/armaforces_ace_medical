class ACE_Medical_Advanced {
    // Defines all the possible treatment types for advanced medical
    class Treatment {
        class Bandaging {
            // Field dressing is normal average treatment
            // packing bandage is average treatment, higher reopen chance, longer reopening delay
            // elastic bandage is higher treatment, higher reopen chance, shorter reopen delay
            // quickclot is lower treatment, lower reopen chance, longer reopening delay
            class Bandage { // basic bandage
                effectiveness = 10;
                reopeningChance = 0;
                reopeningMinDelay = 0;
                reopeningMaxDelay = 0;
            };

            class FieldDressing {
                // How effect is the bandage for treating one wounds type injury
                effectiveness = 1;
                // What is the chance and delays (in seconds) of the treated default injury reopening
                reopeningChance = 0.1;
                reopeningMinDelay = 120;
                reopeningMaxDelay = 200;

                // Super efficiency
                class Abrasion {
                    effectiveness = 10;
                    reopeningChance = 0.1;
                    reopeningMinDelay = 600;
                    reopeningMaxDelay = 1800;
                };
                class AbrasionMinor: Abrasion {
                    effectiveness = 10;
                };
                class AbrasionMedium: Abrasion {
                    effectiveness = 8;
                    reopeningChance = 0.15;
                };
                class AbrasionLarge: Abrasion {
                    effectiveness = 7;
                    reopeningChance = 0.2;
                };

                // Standard efficiency
                class Avulsions: Abrasion {
                    effectiveness = 4;
                    reopeningChance = 0.4;
                    reopeningMinDelay = 300;
                    reopeningMaxDelay = 600;
                };
                class AvulsionsMinor: Avulsions {
                    effectiveness = 4;
                };
                class AvulsionsMedium: Avulsions {
                    effectiveness = 3;
                };
                class AvulsionsLarge: Avulsions {
                    effectiveness = 2;
                };

                // Standard efficiency
                class Contusion: Abrasion {
                    effectiveness = 4;
                    reopeningChance = 0;
                    reopeningMinDelay = 0;
                    reopeningMaxDelay = 0;
                };
                class ContusionMinor: Contusion {};
                class ContusionMedium: Contusion {};
                class ContusionLarge: Contusion {};

                // Standard efficiency
                class CrushWound: Abrasion {
                    effectiveness = 4;
                    reopeningChance = 0.2;
                    reopeningMinDelay = 200;
                    reopeningMaxDelay = 1000;
                };
                class CrushWoundMinor: CrushWound {
                    effectiveness = 4;
                    reopeningChance = 0.2;
                };
                class CrushWoundMedium: CrushWound {
                    effectiveness = 3;
                    reopeningChance = 0.25;
                };
                class CrushWoundLarge: CrushWound {
                    effectiveness = 2;
                    reopeningChance = 0.3;
                };

                // Super efficiency
                class Cut: Abrasion {
                    effectiveness = 10;
                    reopeningChance = 0.1;
                    reopeningMinDelay = 600;
                    reopeningMaxDelay = 1800;
                };
                class CutMinor: Cut {
                    effectiveness = 10;
                    reopeningChance = 0.1;
                };
                class CutMedium: Cut {
                    effectiveness = 9;
                    reopeningChance = 0.2;
                };
                class CutLarge: Cut {
                    effectiveness = 8;
                    reopeningChance = 0.3;
                };

                // Super efficiency
                class Laceration: Abrasion {
                    effectiveness = 10;
                    reopeningChance = 0.2;
                    reopeningMinDelay = 600;
                    reopeningMaxDelay = 1800;
                };
                class LacerationMinor: Laceration {
                    effectiveness = 10;
                    reopeningChance = 0.2;
                };
                class LacerationMedium: Laceration {
                    effectiveness = 9;
                    reopeningChance = 0.3;
                };
                class LacerationLarge: Laceration {
                    effectiveness = 8;
                    reopeningChance = 0.4;
                };

                // Increased efficiency
                class velocityWound: Abrasion {
                    effectiveness = 6;
                    reopeningChance = 0.6;
                    reopeningMinDelay = 600;
                    reopeningMaxDelay = 1000;
                };
                class velocityWoundMinor: velocityWound {
                    effectiveness = 6;
                };
                class velocityWoundMedium: velocityWound {
                    effectiveness = 5;
                };
                class velocityWoundLarge: velocityWound {
                    effectiveness = 4;
                };

                // Increased efficiency
                class punctureWound: Abrasion {
                    effectiveness = 6;
                    reopeningChance = 0.4;
                    reopeningMinDelay = 360;
                    reopeningMaxDelay = 900;
                };
                class punctureWoundMinor: punctureWound {
                    effectiveness = 6;
                };
                class punctureWoundMedium: punctureWound {
                    effectiveness = 5;
                };
                class punctureWoundLarge: punctureWound {
                    effectiveness = 4;
                };
            };

            class PackingBandage: fieldDressing {
                
                // Standard efficiency
                class Abrasion {
                    effectiveness = 4;
                    reopeningChance = 0.5;
                    reopeningMinDelay = 800;
                    reopeningMaxDelay = 1500;
                };
                class AbrasionMinor: Abrasion {
                    effectiveness = 4;
                };
                class AbrasionMedium: Abrasion {
                    effectiveness = 3;
                    reopeningChance = 0.6;
                };
                class AbrasionLarge: Abrasion {
                    effectiveness = 2;
                    reopeningChance = 0.7;
                };

                // Standard efficiency
                class Avulsions: Abrasion {
                    effectiveness = 4;
                    reopeningChance = 0.5;
                    reopeningMinDelay = 1000;
                    reopeningMaxDelay = 1600;
                };
                class AvulsionsMinor: Avulsions {
                    effectiveness = 4;
                };
                class AvulsionsMedium: Avulsions {
                    effectiveness = 3;
                };
                class AvulsionsLarge: Avulsions {
                    effectiveness = 2;
                };

                // Standard efficiency
                class Contusion: Abrasion {
                    effectiveness = 4;
                    reopeningChance = 0;
                    reopeningMinDelay = 0;
                    reopeningMaxDelay = 0;
                };
                class ContusionMinor: Contusion {};
                class ContusionMedium: Contusion {};
                class ContusionLarge: Contusion {};

                class CrushWound: Abrasion {
                    effectiveness = 4;
                    reopeningChance = 0.4;
                    reopeningMinDelay = 600;
                    reopeningMaxDelay = 1000;
                };
                class CrushWoundMinor: CrushWound {
                    effectiveness = 4;
                    reopeningChance = 0.4;
                };
                class CrushWoundMedium: CrushWound {
                    effectiveness = 3;
                    reopeningChance = 0.5;
                };
                class CrushWoundLarge: CrushWound {
                    effectiveness = 2;
                    reopeningChance = 0.6;
                };

                // Standard efficiency
                class Cut: Abrasion {
                    effectiveness = 4;
                    reopeningChance = 0.4;
                    reopeningMinDelay = 700;
                    reopeningMaxDelay = 1000;
                };
                class CutMinor: Cut {
                    effectiveness = 4;
                    reopeningChance = 0.4;
                };
                class CutMedium: Cut {
                    effectiveness = 3;
                    reopeningChance = 0.5;
                };
                class CutLarge: Cut {
                    effectiveness = 2;
                    reopeningChance = 0.6;
                };

                // Standard efficiency
                class Laceration: Abrasion {
                    effectiveness = 4;
                    reopeningChance = 0.6;
                    reopeningMinDelay = 500;
                    reopeningMaxDelay = 2000;
                };
                class LacerationMinor: Laceration {
                    effectiveness = 4;
                    reopeningChance = 0.7;
                };
                class LacerationMedium: Laceration {
                    effectiveness = 3;
                    reopeningChance = 0.8;
                };
                class LacerationLarge: Laceration {
                    effectiveness = 2;
                    reopeningChance = 0.9;
                };

                // Super efficiency
                class velocityWound: Abrasion {
                    effectiveness = 10;
                    reopeningChance = 0.3;
                    reopeningMinDelay = 1000;
                    reopeningMaxDelay = 2000;
                };
                class velocityWoundMinor: velocityWound {
                    effectiveness = 10;
                };
                class velocityWoundMedium: velocityWound {
                    effectiveness = 9;
                };
                class velocityWoundLarge: velocityWound {
                    effectiveness = 8;
                };

                // Super efficiency
                class punctureWound: Abrasion {
                    effectiveness = 10;
                    reopeningChance = 0.3;
                    reopeningMinDelay = 1000;
                    reopeningMaxDelay = 3000;
                };
                class punctureWoundMinor: punctureWound {
                    effectiveness = 10;
                };
                class punctureWoundMedium: punctureWound {
                    effectiveness = 9;
                };
                class punctureWoundLarge: punctureWound {
                    effectiveness = 8;
                };
            };

            class ElasticBandage: fieldDressing {
                
                // Standard efficiency
                class Abrasion {
                    effectiveness = 4;
                    reopeningChance = 0.5;
                    reopeningMinDelay = 300;
                    reopeningMaxDelay = 600;
                };
                class AbrasionMinor: Abrasion {
                    effectiveness = 4;
                };
                class AbrasionMedium: Abrasion {
                    effectiveness = 3;
                    reopeningChance = 0.6;
                };
                class AbrasionLarge: Abrasion {
                    effectiveness = 2;
                    reopeningChance = 0.7;
                };

                // Super efficiency
                class Avulsions: Abrasion {
                    effectiveness = 10;
                    reopeningChance = 0.1;
                    reopeningMinDelay = 1000;
                    reopeningMaxDelay = 1600;
                };
                class AvulsionsMinor: Avulsions {
                    effectiveness = 10;
                };
                class AvulsionsMedium: Avulsions {
                    effectiveness = 9;
                };
                class AvulsionsLarge: Avulsions {
                    effectiveness = 8;
                };

                // Super efficiency
                class Contusion: Abrasion {
                    effectiveness = 10;
                    reopeningChance = 0;
                    reopeningMinDelay = 0;
                    reopeningMaxDelay = 0;
                };
                class ContusionMinor: Contusion {};
                class ContusionMedium: Contusion {};
                class ContusionLarge: Contusion {};

                // Super efficiency
                class CrushWound: Abrasion {
                    effectiveness = 10;
                    reopeningChance = 0.1;
                    reopeningMinDelay = 600;
                    reopeningMaxDelay = 1000;
                };
                class CrushWoundMinor: CrushWound {
                    effectiveness = 10;
                    reopeningChance = 0.1;
                };
                class CrushWoundMedium: CrushWound {
                    effectiveness = 9;
                    reopeningChance = 0.15;
                };
                class CrushWoundLarge: CrushWound {
                    effectiveness = 8;
                    reopeningChance = 0.2;
                };

                // Standard efficiency
                class Cut: Abrasion {
                    effectiveness = 4;
                    reopeningChance = 0.5;
                    reopeningMinDelay = 70;
                    reopeningMaxDelay = 100;
                };
                class CutMinor: Cut {
                    effectiveness = 4;
                    reopeningChance = 0.5;
                };
                class CutMedium: Cut {
                    effectiveness = 3;
                    reopeningChance = 0.6;
                };
                class CutLarge: Cut {
                    effectiveness = 2;
                    reopeningChance = 0.7;
                };
                
                // Standard efficiency
                class Laceration: Abrasion {
                    effectiveness = 4;
                    reopeningChance = 0.6;
                    reopeningMinDelay = 300;
                    reopeningMaxDelay = 600;
                };
                class LacerationMinor: Laceration {
                    effectiveness = 4;
                    reopeningChance = 0.6;
                };
                class LacerationMedium: Laceration {
                    effectiveness = 3;
                    reopeningChance = 0.7;
                };
                class LacerationLarge: Laceration {
                    effectiveness = 2;
                    reopeningChance = 0.8;
                };

                // Increased efficiency
                class velocityWound: Abrasion {
                    effectiveness = 6;
                    reopeningChance = 0.6;
                    reopeningMinDelay = 600;
                    reopeningMaxDelay = 1000;
                };
                class velocityWoundMinor: velocityWound {
                    effectiveness = 6;
                };
                class velocityWoundMedium: velocityWound {
                    effectiveness = 5;
                };
                class velocityWoundLarge: velocityWound {
                    effectiveness = 4;
                };

                // Increased efficiency
                class punctureWound: Abrasion {
                    effectiveness = 6;
                    reopeningChance = 0.4;
                    reopeningMinDelay = 360;
                    reopeningMaxDelay = 900;
                };
                class punctureWoundMinor: punctureWound {
                    effectiveness = 6;
                };
                class punctureWoundMedium: punctureWound {
                    effectiveness = 5;
                };
                class punctureWoundLarge: punctureWound {
                    effectiveness = 4;
                };
            };

            class QuikClot: fieldDressing {
                // Standard efficiency
                class Abrasion {
                    effectiveness = 4;
                    reopeningChance = 0.3;
                    reopeningMinDelay = 800;
                    reopeningMaxDelay = 1500;
                };
                class AbrasionMinor: Abrasion {
                    effectiveness = 4;
                };
                class AbrasionMedium: Abrasion {
                    effectiveness = 3;
                    reopeningChance = 0.4;
                };
                class AbrasionLarge: Abrasion {
                    effectiveness = 2;
                    reopeningChance = 0.5;
                };
                
                // Standard efficiency
                class Avulsions: Abrasion {
                    effectiveness = 4;
                    reopeningChance = 0.2;
                    reopeningMinDelay = 1000;
                    reopeningMaxDelay = 1600;
                };
                class AvulsionsMinor: Avulsions {
                    effectiveness = 4;
                };
                class AvulsionsMedium: Avulsions {
                    effectiveness = 3;
                };
                class AvulsionsLarge: Avulsions {
                    effectiveness = 2;
                };
                
                // Standard efficiency
                class Contusion: Abrasion {
                    effectiveness = 4;
                    reopeningChance = 0;
                    reopeningMinDelay = 0;
                    reopeningMaxDelay = 0;
                };
                class ContusionMinor: Contusion {};
                class ContusionMedium: Contusion {};
                class ContusionLarge: Contusion {};
                
                // Standard efficiency
                class CrushWound: Abrasion {
                    effectiveness = 4;
                    reopeningChance = 0.5;
                    reopeningMinDelay = 600;
                    reopeningMaxDelay = 1000;
                };
                class CrushWoundMinor: CrushWound {
                    effectiveness = 4;
                    reopeningChance = 0.3;
                };
                class CrushWoundMedium: CrushWound {
                    effectiveness = 3;
                };
                class CrushWoundLarge: CrushWound {
                    effectiveness = 2;
                };
                
                // Standard efficiency
                class Cut: Abrasion {
                    effectiveness = 2;
                    reopeningChance = 0.2;
                    reopeningMinDelay = 700;
                    reopeningMaxDelay = 1000;
                };
                class CutMinor: Cut {
                    effectiveness = 2;
                    reopeningChance = 0.3;
                };
                class CutMedium: Cut {
                    effectiveness = 1;
                };
                class CutLarge: Cut {
                    effectiveness = 0.6;
                };
                
                // Standard efficiency
                class Laceration: Abrasion {
                    effectiveness = 4;
                    reopeningChance = 0.4;
                    reopeningMinDelay = 500;
                    reopeningMaxDelay = 2000;
                };
                class LacerationMinor: Laceration {
                    effectiveness = 4;
                    reopeningChance = 0.4;
                };
                class LacerationMedium: Laceration {
                    effectiveness = 3;
                };
                class LacerationLarge: Laceration {
                    effectiveness = 2;
                };

                // Super efficiency
                class velocityWound: Abrasion {
                    effectiveness = 10;
                    reopeningChance = 0.1;
                    reopeningMinDelay = 1000;
                    reopeningMaxDelay = 2000;
                };
                class velocityWoundMinor: velocityWound {
                    effectiveness = 10;
                };
                class velocityWoundMedium: velocityWound {
                    effectiveness = 9;
                };
                class velocityWoundLarge: velocityWound {
                    effectiveness = 8;
                };

                // Super efficiency
                class punctureWound: Abrasion {
                    effectiveness = 10;
                    reopeningChance = 0.1;
                    reopeningMinDelay = 1000;
                    reopeningMaxDelay = 3000;
                };
                class punctureWoundMinor: punctureWound {
                    effectiveness = 10;
                };
                class punctureWoundMedium: punctureWound {
                    effectiveness = 9;
                };
                class punctureWoundLarge: punctureWound {
                    effectiveness = 8;
                };
            };
        };
        class Medication {
            // How much does the pain get reduced?
            painReduce = 0;
            // How much will the heart rate be increased when the HR is low (below 55)? {minIncrease, maxIncrease, seconds}
            hrIncreaseLow[] = {0, 0, 0};
            hrIncreaseNormal[] = {0, 0, 0};
            hrIncreaseHigh[] = {0, 0, 0};
            // Callback once the heart rate values have been added.
            hrCallback = "";

            // How long until this medication has disappeared
            timeInSystem = 120;
            // How many of this type of medication can be in the system before the patient overdoses?
            maxDose = 4;
            // Function to execute upon overdose. Arguments passed to call back are 0: unit <OBJECT>, 1: medicationClassName <STRING>
            onOverDose = "";
            // The viscosity of a fluid is a measure of its resistance to gradual deformation by shear stress or tensile stress. For liquids, it corresponds to the informal concept of "thickness". This value will increase/decrease the viscoty of the blood with the percentage given. Where 100 = max. Using the minus will decrease viscosity
            viscosityChange = 0;

            // specific details for the ACE_Morphine treatment action
            class Morphine {
                painReduce = 50;
                hrIncreaseLow[] = {-10, -20, 35};
                hrIncreaseNormal[] = {-10, -30, 35};
                hrIncreaseHigh[] = {-10, -35, 50};
                timeInSystem = 3600;
                maxDose = 4;
                inCompatableMedication[] = {};
                viscosityChange = -10;
            };
            class Fentanyl {
                painReduce = 5000;
                hrIncreaseLow[] = {-10, -20, 35};
                hrIncreaseNormal[] = {-10, -30, 35};
                hrIncreaseHigh[] = {-10, -35, 50};
                timeInSystem = 1800;
                maxDose = 2;
                inCompatableMedication[] = {};
                viscosityChange = -5;
            };
            class Epinephrine {
                painReduce = 0;
                hrIncreaseLow[] = {30, 60, 5};
                hrIncreaseNormal[] = {35, 60, 5};
                hrIncreaseHigh[] = {40, 60, 5};
                timeInSystem = 240;
                maxDose = 2;
                inCompatableMedication[] = {};
                viscosityChange = 50;
            };
            class Adenosine {
                painReduce = 0;
                hrIncreaseLow[] = {0, 0, 0};
                hrIncreaseNormal[] = {0, 0, 0};
                hrIncreaseHigh[] = {0, 0, 0};
                timeInSystem = 20;
                maxDose = 1;
                inCompatableMedication[] = {};
            };
            class Atropine {
                painReduce = 0;
                hrIncreaseLow[] = {7, 10, 15};
                hrIncreaseNormal[] = {15, 25, 20};
                hrIncreaseHigh[] = {15, 30, 10};
                timeInSystem = 120;
                maxDose = 6;
                inCompatableMedication[] = {};
                viscosityChange = -10;
            };
            class Naloxone {
                painReduce = 0;
                hrIncreaseLow[] = {30, 60, 100};
                hrIncreaseNormal[] = {30, 90, 100};
                hrIncreaseHigh[] = {30, 90, 120};
                timeInSystem = 120;
                maxDose = 6;
                inCompatableMedication[] = {};
            };
            class PainKillers {
                painReduce = 0.7;
                timeInSystem = 120;
                maxDose = 10;
                inCompatableMedication[] = {};
                viscosityChange = 5;
            };
            class Apap: PainKillers {
                painReduce = 5;
                timeInSystem = 300;
                maxDose = 10;
                inCompatableMedication[] = {};
                viscosityChange = 5;
            };
        };
    };
};