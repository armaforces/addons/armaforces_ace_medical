                class ACE_Head {
                    class fieldDressing;
                    class Apap: fieldDressing {
                        displayName = $STR_ArmaForces_Medical_Inject_Apap;
                        condition = "[_player, _target, 'head', 'Apap'] call ace_medical_fnc_canTreatCached";
                        statement = "[_player, _target, 'head', 'Apap'] call ace_medical_fnc_treatment";
                        icon = SET_ICON(apap);
                    };
                };
                class ACE_ArmLeft {
                    class Morphine;
                    class Fentanyl: Morphine {
                        displayName = $STR_ArmaForces_Medical_Inject_Fentanyl;
                        condition = "[_player, _target, 'hand_l', 'Fentanyl'] call ace_medical_fnc_canTreatCached";
                        statement = "[_player, _target, 'hand_l', 'Fentanyl'] call ace_medical_fnc_treatment";
                    };
                    class Naloxone: Morphine {
                        displayName = $STR_ArmaForces_Medical_Inject_Naloxone;
                        condition = "[_player, _target, 'hand_l', 'Naloxone'] call ace_medical_fnc_canTreatCached";
                        statement = "[_player, _target, 'hand_l', 'Naloxone'] call ace_medical_fnc_treatment";
                    };
                };
                class ACE_ArmRight {
                    class Morphine;
                    class Fentanyl: Morphine {
                        displayName = $STR_ArmaForces_Medical_Inject_Fentanyl;
                        condition = "[_player, _target, 'hand_r', 'Fentanyl'] call ace_medical_fnc_canTreatCached";
                        statement = "[_player, _target, 'hand_r', 'Fentanyl'] call ace_medical_fnc_treatment";
                    };
                    class Naloxone: Morphine {
                        displayName = $STR_ArmaForces_Medical_Inject_Naloxone;
                        condition = "[_player, _target, 'hand_r', 'Naloxone'] call ace_medical_fnc_canTreatCached";
                        statement = "[_player, _target, 'hand_r', 'Naloxone'] call ace_medical_fnc_treatment";
                    };
                };
                class ACE_LegLeft {
                    class Morphine;
                    class Fentanyl: Morphine {
                        displayName = $STR_ArmaForces_Medical_Inject_Fentanyl;
                        condition = "[_player, _target, 'leg_l', 'Fentanyl'] call ace_medical_fnc_canTreatCached";
                        statement = "[_player, _target, 'leg_l', 'Fentanyl'] call ace_medical_fnc_treatment";
                    };
                    class Naloxone: Morphine {
                        displayName = $STR_ArmaForces_Medical_Inject_Naloxone;
                        condition = "[_player, _target, 'leg_l', 'Naloxone'] call ace_medical_fnc_canTreatCached";
                        statement = "[_player, _target, 'leg_l', 'Naloxone'] call ace_medical_fnc_treatment";
                    };
                };
                class ACE_LegRight {
                    class Morphine;
                    class Fentanyl: Morphine {
                        displayName = $STR_ArmaForces_Medical_Inject_Fentanyl;
                        condition = "[_player, _target, 'leg_r', 'Fentanyl'] call ace_medical_fnc_canTreatCached";
                        statement = "[_player, _target, 'leg_r', 'Fentanyl'] call ace_medical_fnc_treatment";
                    };
                    class Naloxone: Morphine {
                        displayName = $STR_ArmaForces_Medical_Inject_Naloxone;
                        condition = "[_player, _target, 'leg_r', 'Naloxone'] call ace_medical_fnc_canTreatCached";
                        statement = "[_player, _target, 'leg_r', 'Naloxone'] call ace_medical_fnc_treatment";
                    };
                };