#define MACRO_ADDITEM(ITEM,COUNT) class _xx_##ITEM { \
    name = #ITEM; \
    count = COUNT; \
}


class CfgVehicles {
    class Item_Base_F;
    class ACE_fentanylItem: Item_Base_F {
        scope = 2;
        scopeCurator = 2;
        displayName = $STR_ArmaForces_Medical_Fentanyl_Display;
        author = "ArmaForces";
        vehicleClass = "Items";
        class TransportItems {
            MACRO_ADDITEM(ACE_fentanyl,1);
        };
    };
    class ACE_naloxonelItem: Item_Base_F {
        scope = 2;
        scopeCurator = 2;
        displayName = $STR_ArmaForces_Medical_Naloxone_Display;
        author = "ArmaForces";
        vehicleClass = "Items";
        class TransportItems {
            MACRO_ADDITEM(ACE_naloxone,1);
        };
    };
    class ACE_apapItem: Item_Base_F {
        scope = 2;
        scopeCurator = 2;
        displayName = $STR_ArmaForces_Medical_Apap_Display;
        author = "ArmaForces";
        icon = SET_ICON(apap);
        model = "\A3\Structures_F_EPA\Items\Medical\PainKillers_F.p3d";
        vehicleClass = "Items";
        class TransportItems {
            MACRO_ADDITEM(ACE_apap,1);
        };
    };

    class Leaflet_05_F;
    class ACE_MedicalLitter_apap: Leaflet_05_F {
        hiddenSelectionsTextures[] = {"\armaforces\medical\data\apap_litter.paa"};
    };

    class Man;
    class CAManBase: Man {
        class ACE_Actions {
            class ACE_MainActions {
                class Medical {
                    #include "ACE_Medical_Actions.hpp"
                };
            };
        };
        class ACE_SelfActions {
            class Medical {
                #include "ACE_Medical_Actions.hpp"
            };
        };
    };

    class NATO_Box_Base;
    class ACE_medicalSupplyCrate: NATO_Box_Base {
        class TransportItems;
        class TransportItems {
            MACRO_ADDITEM(ACE_apap,50);
        };
    };
    class ACE_medicalSupplyCrate_advanced: ACE_medicalSupplyCrate {
        class TransportItems;
        class TransportItems {
            MACRO_ADDITEM(ACE_apap,50);
            MACRO_ADDITEM(ACE_fentanyl,15);
            MACRO_ADDITEM(ACE_naloxone,15);
        };
    };
    class ACE_medicalSupplyCrate_armaforces: ACE_medicalSupplyCrate_advanced {
        displayName = $STR_ArmaForces_Medical_SupplyCrate_Armaforces;
        class TransportItems {
            MACRO_ADDITEM(ACE_apap,50);
            MACRO_ADDITEM(ACE_fieldDressing,40);
            MACRO_ADDITEM(ACE_packingBandage,20);
            MACRO_ADDITEM(ACE_elasticBandage,20);
            MACRO_ADDITEM(ACE_quikClot,20);
            MACRO_ADDITEM(ACE_tourniquet,10);
            MACRO_ADDITEM(ACE_morphine,5);
            MACRO_ADDITEM(ACE_adenosine,5);
            MACRO_ADDITEM(ACE_atropine,5);
            MACRO_ADDITEM(ACE_epinephrine,10);
            MACRO_ADDITEM(ACE_fentanyl,5);
            MACRO_ADDITEM(ACE_naloxone,5);
            MACRO_ADDITEM(ACE_bloodIV,5);
            MACRO_ADDITEM(ACE_bloodIV_500,15);
            MACRO_ADDITEM(ACE_bloodIV_250,20);
        };
    };
};