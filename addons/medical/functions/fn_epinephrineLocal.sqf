params ["", "_target", "", ""];

// Randomize effect
private _efficiencyHR = random [50,55,60];
private _efficiencyResistance = random [35,40,50];
private _duration = random [2*60, 3.5*60, 5*60];

// If HR is present, aplly effect
_HR = _target getVariable ["ace_medical_heartRate", 80];
if (_HR != 0) then {
    _resistance = _target getVariable ["ace_medical_peripheralResistance", 100];
    _target setVariable ["ace_medical_heartRate", _HR + _efficiencyHR];
    _efficiencyHR = 0;
    _target setVariable ["ace_medical_peripheralResistance", _resistance + _efficiencyResistance];
    _efficiencyResistance = 0;
};

// Decrease effect linearly over _duration
private _changeHR = _efficiencyHR/_duration;
private _changeResistance = _efficiencyResistance/_duration;

// Start loop
[armaforces_ace_medical_fnc_epinephrineLocalEffectLoop, [_target, _changeHR, _changeResistance, _duration, _efficiencyHR, _efficiencyResistance], 1] call CBA_fnc_waitAndExecute;