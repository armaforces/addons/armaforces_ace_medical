params ["_target", "_changeHR", "_changeResistance", "_duration", "_efficiencyHR", "_efficiencyResistance"];

// If the unit died the loop is finished
if (!alive _target) exitWith {};
// If locality changed finish the local loop
if (!local _target) exitWith {};

_HR = _target getVariable ["ace_medical_heartRate", 80];
if (_HR != 0) then {
    _resistance = _target getVariable ["ace_medical_peripheralResistance", 100];
    // If HR present and effect was not added, add it
    if (_efficiencyHR > 0) then {
        _target setVariable ["ace_medical_heartRate", _HR + _efficiencyHR];
        _efficiencyHR = 0;
        _target setVariable ["ace_medical_peripheralResistance", _resistance + _efficiencyResistance];
        _efficiencyResistance = 0;
    };
    // Decrease effect
    _target setVariable ["ace_medical_heartRate", _HR - _changeHR];
    _target setVariable ["ace_medical_peripheralResistance", _resistance - _changeResistance];
} else {
    if (_efficiencyHR > 0) then {
        // Decrease delayed effect by fade factor
        _efficiencyHR = _efficiencyHR - _changeHR;
        _efficiencyResistance = _efficiencyResistance - _changeResistance;
    } else {
        // If effect was added, decrease it
        _resistance = _target getVariable ["ace_medical_peripheralResistance", 100];
        _target setVariable ["ace_medical_peripheralResistance", _resistance - _changeResistance];
    };
};

_duration = _duration - 1;
if (_duration == 0) exitWith {};

// Schedule the loop to be executed again 1 sec later
[armaforces_ace_medical_fnc_epinephrineLocalEffectLoop, [_target, _changeHR, _changeResistance, _duration, _efficiencyHR, _efficiencyResistance], 1] call CBA_fnc_waitAndExecute;