class ACE_Medical_Actions
{
    class Advanced
    {
        class FieldDressing
        {
            treatmentTime = 6.5;
        };
        class Tourniquet: FieldDressing
        {
            treatmentTime = 2.5;
        };
        class Morphine;
        class Apap: Morphine {
            allowedSelections[] = {"head"};
            displayName = $STR_ArmaForces_Medical_Inject_Apap;
            displayNameProgress = $STR_ArmaForces_Medical_Injecting_Apap;
            items[] = {"ACE_apap"};
            litter[] = { {"All", "", {"ACE_MedicalLitter_apap"}} };
        };
        class Fentanyl: Morphine {
            displayName = $STR_ArmaForces_Medical_Inject_Fentanyl;
            displayNameProgress = $STR_ArmaForces_Medical_Injecting_Fentanyl;
            items[] = {"ACE_fentanyl"};
        };
        class Atropine;
        class Naloxone: Atropine {
            displayName = $STR_ArmaForces_Medical_Inject_Naloxone;
            displayNameProgress = $STR_ArmaForces_Medical_Injecting_Naloxone;
            items[] = {"ACE_naloxone"};
        };
        class Adenosine: Morphine {
            callbackSuccess = armaforces_ace_medical_fnc_adenosine;
        };
        /*class Epinephrine: Morphine {
            callbackSuccess = armaforces_ace_medical_fnc_epinephrine;
        };*/
        class BloodIV: FieldDressing
        {
            treatmentTime = 5;
        };
        class SurgicalKit: FieldDressing
        {
            treatmentTime = '(count ((_this select 1) getVariable ["ACE_Medical_bandagedWounds", []]) * 4) / (2^((_this select 0) getVariable ["ACE_Medical_medicClass", getNumber (configFile >> "CfgVehicles" >> typeOf (_this select 0) >> "attendant")]))';
        };
        class RemoveTourniquet: Tourniquet
        {
            treatmentTime = 1.5;
        };
        class CPR: FieldDressing
        {
            treatmentTime = 10;
        };
        class BodyBag: FieldDressing
        {
            treatmentTime = 10;
        };
    };
};